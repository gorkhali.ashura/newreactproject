import React from "react";

const AboutAsarfi = () => {
    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height: '100vh'
            }}
        >
            <h1>Asarfi is a Software Company based in Pokhara.</h1>
        </div>
    );
};

export default AboutAsarfi;