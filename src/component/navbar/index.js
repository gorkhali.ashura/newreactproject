import React from "react";
import './nav.css';
import Asarfilogo from './image/asarfilogo.png';

import {
    Nav,
    NavLink,
    Bars,
    NavMenu,
} from "./NavbarElements";

const Navbar = () => {
    return (
        <>
            <Nav>
                <Bars/>
                    <img className="logo" src={Asarfilogo} alt="Logo"/>
            <NavMenu>
                <NavLink to="/home" activeStyle>
                    Home
                </NavLink>
                <NavLink to="/aboutasarfi" activeStyle>
                    About Asarfi
                </NavLink>
                <NavLink to="/aboutteam" activeStyle>
                    About Team
                </NavLink>
                <NavLink  to="/contact" activeStyle>
                    Contact
                </NavLink>
            </NavMenu> 
           </Nav> 
        </>
    );
};
export default Navbar;